package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskStartStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Task start by Index.";

    @NotNull
    private final String NAME = "task-start-by-index";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
